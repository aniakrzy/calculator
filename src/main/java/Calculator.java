public class Calculator {

    public static void main(String[] args) {
        System.out.println(dodawanie(2, 3));
        System.out.println(odejmowanie(2, 3));
        System.out.println(mnozenie(2, 3));
        dzielenie(2, 4);
    }

    public static int dodawanie(int a, int b) {
        return a + b;
    }

    public static int odejmowanie(int a, int b) {
        return a - b;
    }

    public static int mnozenie(int a, int b) {
        return a * b;
    }

    public static void dzielenie(int a, int b) {
        if (b == 0) {
            System.out.println("Dzielenie przez 0!");
        } else {
            double result = (double) a / b;
            System.out.println(result);
        }
    }

    public static double potegowanie(int a, int b){
        return Math.pow(a, b);
    }

}


